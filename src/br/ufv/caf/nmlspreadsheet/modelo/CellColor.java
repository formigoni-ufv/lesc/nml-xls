package br.ufv.caf.nmlspreadsheet.modelo;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.IndexedColors;

public class CellColor {
	private Colors color;

	public CellColor(Colors color){
		this.color = color;
	}

	private CellStyle pinkBackground(HSSFWorkbook workbook){
		CellStyle style = workbook.createCellStyle();
		style.setFillForegroundColor(IndexedColors.PINK.index);
		style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		return style;
	}

	private CellStyle greenBackground(HSSFWorkbook workbook){
		CellStyle style = workbook.createCellStyle();
		style.setFillForegroundColor(IndexedColors.GREEN.index);
		style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		return style;
	}

	private CellStyle blueBackground(HSSFWorkbook workbook){
		CellStyle style = workbook.createCellStyle();
		style.setFillForegroundColor(IndexedColors.BLUE.index);
		style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		return style;
	}

	private CellStyle yellowBackground(HSSFWorkbook workbook){
		CellStyle style = workbook.createCellStyle();
		style.setFillForegroundColor(IndexedColors.YELLOW.index);
		style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		return style;
	}

	private CellStyle yellowDarkBackground(HSSFWorkbook workbook){
		CellStyle style = workbook.createCellStyle();
		style.setFillForegroundColor(IndexedColors.DARK_YELLOW.index);
		style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		return style;
	}

	public CellStyle cellSetColor(HSSFWorkbook workbook){
		if(color.equals(Colors.PINK)){
			return pinkBackground(workbook);
		}else if(color.equals(Colors.GREEN)) {
			return greenBackground(workbook);
		}else if(color.equals(Colors.YELLOW)) {
			return yellowBackground(workbook);
		}else if(color.equals(Colors.YELLOWDARK)){
			return yellowDarkBackground(workbook);
		}else {
			return blueBackground(workbook);
		}
	}
}
