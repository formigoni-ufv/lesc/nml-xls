package br.ufv.caf.nmlspreadsheet.modelo;

public class Magnet {
	private boolean fixedMagnetization;
	private double magnetization;
	private double clockZone;
	private Colors color;
	private String logic;
	private String id;
	private String type;

	public Magnet(boolean fixedMagnetization, double magnetization, double clockZone, String logic, String id, String type){
		this.fixedMagnetization = fixedMagnetization;
		this.magnetization = magnetization;
		this.clockZone = clockZone;
		this.color = color;
		this.logic = logic;
		this.id = id;
		this.type = type;
	}

	public boolean isFixedMagnetization() {
		return fixedMagnetization;
	}

	public double getMagnetization() {
		return magnetization;
	}

	public double getClockZone() {
		return clockZone;
	}

	public Colors getColor() {
		return color;
	}

	public String getLogic() {
		return logic;
	}

	public String getId() {
		return id;
	}

	public String getType() {
		return type;
	}

	public void setFixedMagnetization(boolean fixedMagnetization) {
		this.fixedMagnetization = fixedMagnetization;
	}

	public void setMagnetization(double magnetization) {
		this.magnetization = magnetization;
	}

	public void setClockZone(double clockZone) {
		this.clockZone = clockZone;
	}

	public void setColor(Colors color) {
		this.color = color;
	}

	public void setLogic(String logic) {
		this.logic = logic;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void printMagnetData(Magnet magnet){
		System.out.println("Fixed Magnetization: "+magnet.isFixedMagnetization());
		System.out.println("Magnetization: "+magnet.getMagnetization());
		System.out.println("Clock Zone: "+magnet.getClockZone());
		System.out.println("Logic: "+magnet.getLogic());
		System.out.println("Id: "+magnet.getId());
		System.out.println("Type: "+magnet.getType());
	}
}
