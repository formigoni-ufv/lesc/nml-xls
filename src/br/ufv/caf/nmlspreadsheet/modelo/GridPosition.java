package br.ufv.caf.nmlspreadsheet.modelo;

public class GridPosition {
	private short row;
	private short column;

	public short getRow() {
		return row;
	}

	public short getColumn() {
		return column;
	}

	public void setRow(short row) {
		this.row = row;
	}

	public void setColumn(short column) {
		this.column = column;
	}

	public void printData(){
		System.out.println("Line: " + this.row + "\n" + "Column: " + this.column);
	}
}
