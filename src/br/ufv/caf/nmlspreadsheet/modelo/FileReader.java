package br.ufv.caf.nmlspreadsheet.modelo;

import java.io.*;
import java.util.ArrayList;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;

public class FileReader {
	private String filename;
	private java.io.FileReader frFile;
	private BufferedReader buffFile;
	private ArrayList<Magnet> magnets;
	private ArrayList<GridPosition> gridPositions;

	public FileReader(String filename) {
		magnets = new ArrayList<>();
		gridPositions = new ArrayList<>();
		this.filename = filename;
		loadFile();
	}

	private void loadFile() {
		try {
			frFile = new java.io.FileReader(filename);
			buffFile = new BufferedReader(frFile);
			System.out.println("File loaded successfully.");
		} catch (IOException ioe) {
			System.err.println("File does not exist.");
		}
		getMagnetsData(buffFile, magnets, gridPositions);
	}

	private void getMagnetsData(BufferedReader buffFile, ArrayList<Magnet> magnets, ArrayList<GridPosition> gridPositions) {
		String line = "empty";
		StringTokenizer lineToken;
		String currentToken;
		Magnet magnet = new Magnet(false, (byte) -1, (byte) -1, null, null, null);
		GridPosition gridPosition = new GridPosition();

		try {
			line = buffFile.readLine();
		} catch (IOException e) {
			System.out.println("Error reading source file (I/O).");
			return;
		} catch (NullPointerException e) {
			System.out.println("Error reading source file (NULL Pointer).");
			return;
		}

		lineToken = new StringTokenizer(line, "\":");

		LOOP:
		do {
			currentToken = lineToken.nextToken();

			switch (currentToken) {
				case "magnetization":
					currentToken = lineToken.nextToken();
					currentToken = currentToken.replaceAll(",", "");
					magnet.setMagnetization(Double.parseDouble(currentToken));
					break;
				case "clock_zone":
					currentToken = lineToken.nextToken();
					currentToken = currentToken.replaceAll(",", "");
					magnet.setClockZone(Double.parseDouble(currentToken));
					break;
				case "fixed_magnetization":
					currentToken = lineToken.nextToken();
					currentToken = currentToken.replaceAll(",", "");
					magnet.setFixedMagnetization(Boolean.parseBoolean(currentToken));
					break;
				case "x":
					currentToken = lineToken.nextToken();
					currentToken = currentToken.replaceAll(",", "");
					currentToken = currentToken.substring(0, currentToken.indexOf("."));
					gridPosition.setColumn(Short.parseShort(currentToken));
					break;
				case "y":
					currentToken = lineToken.nextToken();
					currentToken = currentToken.replaceAll(",", "");
					currentToken = currentToken.substring(0, currentToken.indexOf("."));
					gridPosition.setRow(Short.parseShort(currentToken));
					break;
				case "logic":
					currentToken = lineToken.nextToken();
					magnet.setLogic(currentToken);
					break;
				case "type":
					currentToken = lineToken.nextToken();
					magnet.setType(currentToken);
					break;
				case "id":
					currentToken = lineToken.nextToken();
					if (!currentToken.equals(",")) magnet.setId(currentToken);
					else magnet.setId("none");
					break;
				case "},{":
					magnets.add(magnet);
					gridPositions.add(gridPosition);
					magnet = new Magnet(false, (byte) -1, (byte) -1, null, null, null);
					gridPosition = new GridPosition();
					break;
				case "}]":
					break LOOP;
			}
		} while (lineToken.hasMoreElements());
	}

	public int getMagnetsCount() {
		return magnets.size();
	}

	public Magnet getMagnet(int index) {
		return magnets.get(index);
	}

	public GridPosition getMagnetPosition(int index) {
		return gridPositions.get(index);
	}
}
