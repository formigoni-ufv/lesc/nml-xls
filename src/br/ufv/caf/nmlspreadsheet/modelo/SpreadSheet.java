package br.ufv.caf.nmlspreadsheet.modelo;

//Find jar from here "http://poi.apache.org/download.html"

import java.io.*;

import org.apache.poi.hssf.usermodel.*;

public class SpreadSheet {
	private String name;
	private short rows;
	private short columns;
	private HSSFWorkbook workbook;
	private HSSFSheet sheet;

	public SpreadSheet(String name, short rows, short columns, int width, short height) {
		this.name = name;
		this.rows = rows;
		this.columns = columns;
		this.workbook = new HSSFWorkbook();
		this.sheet = workbook.createSheet(name);
		setWidth(width);
		setHeight(height);
		loadFile();
	}

	private void loadFile() {
		for (short i = 0; i < rows; i++) {
			this.sheet.createRow(i);
			for (short j = 0; j < columns; j++) {
				this.sheet.getRow(i).createCell(j);
//				setCellValue("1", i, j);
//				setCellColor(Colors.RED, i, j);
			}
		}
	}

	public void saveFile(String outputFileName) {
		outputFileName = outputFileName + ".xls";
		try {
			FileOutputStream fileOut = new FileOutputStream(outputFileName);
			workbook.write(fileOut);
			fileOut.close();
			System.out.println("Your excel file has been generated!");
		} catch (Exception ex) {
			System.err.println(ex);
		}
	}

	public void setWidth(int width) {
		sheet.setDefaultColumnWidth(width);
	}

	public void setHeight(short height) {
		sheet.setDefaultRowHeight(height);
	}
	public void setCellValue(String value, short row, short column) {
		try {
			sheet.getRow(row).getCell(column).setCellValue(value);
		} catch (NullPointerException ne) {
			System.err.println("Cell does not exist, Goku does.");
		}
	}

	public void setCellColor(Colors color, short row, short column) {
		CellColor cellColors = new CellColor(color);
		try {
			sheet.getRow(row).getCell(column).setCellStyle(cellColors.cellSetColor(workbook));
		} catch (NullPointerException ne) {
			System.err.println("Cell does not exist, Goku does.");
		}
	}

	public String getName() {
		return name;
	}

	public short getRows() {
		return rows;
	}

	public short getColumns() {
		return columns;
	}
}

