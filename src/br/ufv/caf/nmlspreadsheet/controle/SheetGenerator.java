package br.ufv.caf.nmlspreadsheet.controle;

import br.ufv.caf.nmlspreadsheet.modelo.*;

public class SheetGenerator {
	private FileReader nmlfile;
	private SpreadSheet spreadSheet;

	public SheetGenerator(){
		nmlfile = new FileReader("cho");
		spreadSheet = new SpreadSheet("magnetsSheet", (short)200, (short)200, 7, (short) 1200);
		addMagnets();
	}

	private void addMagnets(){
		Magnet magnet;
		GridPosition position;

		int magnetCount = nmlfile.getMagnetsCount();

		for(int i=0; i<magnetCount; i++) {
			magnet = nmlfile.getMagnet(i);
			position = nmlfile.getMagnetPosition(i);

			if (magnet.getType().equals("output")){
				spreadSheet.setCellColor(Colors.YELLOW, position.getRow(), position.getColumn());
				spreadSheet.setCellValue(magnet.getId(), position.getRow(), position.getColumn());
			}else if(magnet.getType().equals("input")) {
				spreadSheet.setCellColor(Colors.YELLOWDARK, position.getRow(), position.getColumn());
				spreadSheet.setCellValue(magnet.getId(), position.getRow(), position.getColumn());
			}else if(magnet.getClockZone() == 0){
				spreadSheet.setCellColor(Colors.GREEN, position.getRow(), position.getColumn());
			}else if(magnet.getClockZone() == 1){
				spreadSheet.setCellColor(Colors.BLUE, position.getRow(), position.getColumn());
			}else{
				spreadSheet.setCellColor(Colors.PINK, position.getRow(), position.getColumn());
			}
		}

		spreadSheet.saveFile(spreadSheet.getName());
	}
}
